# updated SONJA
# Design and implement a container (docker image)

# Use the official maven/Java 8 image to create a build artifact.
##FROM maven:3.6.3-openjdk-11-slim as builder

# Copy local code to the container image.
##WORKDIR /app
##COPY pom.xml .
##COPY src ./src

# Build a release artifact.
##RUN mvn package

# Use AdoptOpenJDK for base image.
# It's important to use OpenJDK 8 or above that has container support enabled.
# https://hub.docker.com/r/adoptopenjdk/openjdk8
# https://docs.docker.com/develop/develop-images/multistage-build/#use-multi-stage-builds
##FROM adoptopenjdk/openjdk11:alpine-slim

# Copy the jar to the production image from the builder stage.
##COPY --from=builder /app/target/*.jar /app.jar

# Run the web service on container startup.
##CMD ["java", "-jar", "/app.jar"]
