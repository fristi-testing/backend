package com.fristi.fristi.Repositories;

import com.fristi.fristi.models.Registration;
import com.fristi.fristi.repositories.RegistrationRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class RegistrationRepositoryTest
{

    @Autowired
    private RegistrationRepository registrationRepository;

    @Test
    public void injectedComponentsAreNotNull(){
        assertThat(registrationRepository).isNotNull();

    }
    @Test
    public void test() throws Exception
            {registrationRepository.save(new Registration());}

}
