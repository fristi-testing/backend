package com.fristi.fristi.Controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fristi.fristi.controllers.ActivityController;
import com.fristi.fristi.models.Activity;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Random;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.hamcrest.Matchers.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class ActivityControllerTest {

    //  Activity a = new Activity("Tuinfeest 2020", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.", new Date(2020, 11, 24, 14, 30), new Date(2020, 11, 24, 18, 30), true, null, 51,0.0);

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ActivityController activityController;

    @Test
    void contextLoads() throws Exception {
        assertThat(activityController).isNotNull();
    }

    @Test
    void getAllActivities() throws Exception {
        mockMvc.perform(get("/api/activities")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*]", hasSize(5)));
    }

    @Test
    void getOneActivity() throws Exception {
        mockMvc.perform(get("/api/activities/1"))
                .andExpect(jsonPath("$[*]", hasSize(10)))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("Tuinfeest 2020")))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.description", is("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Suspendisse tempus ipsum lacus, sit amet pharetra turpis congue at. Sed at rutrum enim, vel iaculis velit. Donec sem turpis, ultrices ut tortor rutrum, dapibus rhoncus risus. In porttitor elit at quam rhoncus, vel dictum arcu cursus.")))
                .andExpect(jsonPath("$.published", is(true)))
                .andExpect(jsonPath("$.imageLink", is("image1")))
                .andExpect(jsonPath("$.amountOfLikes", is(51)));
    }

    /* ff

    @Test
    void postOneActivity() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        int randomLikes = new Random().nextInt(1000);
        Activity act = new Activity("testName","testDescr",LocalDateTime.of(2020, 11, 24, 14, 30), LocalDateTime.of(2020, 11, 24, 18, 30),false,null,randomLikes,0.0,"Test");
        mockMvc.perform(post("/api/activities").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(act)))
                .andExpect(status().is(201))
                .andExpect(jsonPath("$.imageLink").doesNotExist())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.amountOfLikes", is(randomLikes)));
    }

    */

    //Beatrijs: als we niet met DummyDataInitializer werken kunnen we dit testen
    //vb. in coronatijden: er zijn geen activiteiten gepland ...
//    @Test
//    public void shouldGetEmptyArrayWhenNoActivitiesExist() throws Exception {
//        MvcResult mvcResult = this.mockMvc
//                .perform(get("/api/activities"))
//                .andExpect(status().is(200))
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$.size()", is(0)))
//                //andDo(print()) will print the request and response. This is helpful to get a detailed view in case of error
//                .andDo(print())
//                //andReturn() will return the MvcResult object which is used when we have to verify something which is not achievable by the library.
//                .andReturn();
//
//    }

    @Test
    void getWrongActivity() throws Exception {
        mockMvc.perform(get("/api/activities/9"))
                .andExpect(status().isNotFound())
                //voor meer info over request en response
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }

}
