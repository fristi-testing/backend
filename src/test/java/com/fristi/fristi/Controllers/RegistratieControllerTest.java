package com.fristi.fristi.Controllers;

import com.fristi.fristi.controllers.RegistrationController;
import com.fristi.fristi.models.Activity;
import com.fristi.fristi.models.Registration;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class RegistratieControllerTest {

private List<Registration> registrationsTest;
@Autowired
private MockMvc mockMvc;

@Autowired
private RegistrationController registratieController;

@Test
    void contextLoads() throws Exception{
    assertThat(registratieController).isNotNull();
}

@Test
void getAllRegistrations() throws Exception {
mockMvc.perform(get("/api/reservations")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*]", hasSize(1))

    );
}

    @Test
    void getOneReservation() throws Exception {
        mockMvc.perform(get("/api/reservations/{id}",2
        )
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())

                .andExpect(jsonPath("$[*]", hasSize(6)))
                .andExpect(jsonPath("$.id", is(2)))
                .andExpect(jsonPath("$.amountOfExtraPersons", is(4)))
                .andExpect(jsonPath("$.paymentOffline", is(false)));
               // .andExpect(jsonPath("$.activity",("{id=1, name=Tuinfeest 2020, description=Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat., startDate=2020-11-24T14:30:00, endDate=2020-11-24T18:30:00, published=true, imageLink=image1, amountOfLikes=51, price=0.0, location=Schooltuin}")));
    }

}
