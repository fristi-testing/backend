package com.fristi.fristi.Services;

import com.fristi.fristi.models.Activity;
import com.fristi.fristi.repositories.ActivityRepository;
import com.fristi.fristi.services.ActivityService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDateTime;

import static org.mockito.BDDMockito.given;

@SpringBootTest
public class ActivityServiceTest {

    @Autowired
    private ActivityService activityService;

    @MockBean
    public ActivityRepository activityRepository;

    @Test
    void saveActivity(){
        Activity activity = new Activity("programmeren","blablabla", LocalDateTime.of(2020, 5, 15, 8, 0),LocalDateTime.of(2020, 5, 15, 10,0),true,"",50,20.0, "Station Antwerpen");
        Activity savedActivity = new Activity("programmeren","blablabla",LocalDateTime.of(2020, 5, 15, 8, 0),LocalDateTime.of(2020, 5, 15, 10, 0),true,"",50,20.0, "Oostende");
        savedActivity.setId(5);

        given(activityRepository.save(activity))
                .willReturn(savedActivity);

        System.out.println(activity.toString());
        System.out.println(savedActivity.toString());
    }
}
