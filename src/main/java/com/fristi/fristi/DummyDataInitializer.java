package com.fristi.fristi;

import com.fristi.fristi.models.ApplicationUser;
import com.fristi.fristi.models.Registration;
import com.fristi.fristi.repositories.RegistrationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.fristi.fristi.models.Activity;
import com.fristi.fristi.repositories.ActivityRepository;

@Component
public class DummyDataInitializer implements CommandLineRunner {
    
    private final ActivityRepository activityRepository;
    private final RegistrationRepository registrationRepository;

    @Autowired
    public DummyDataInitializer(ActivityRepository activityRepository, RegistrationRepository registrationRepository) {
        this.activityRepository = activityRepository;
        this.registrationRepository = registrationRepository;
    }

    @Override
    // TODO: Ook voor latere dummy data nog herschrijven met inlezing vanuit een file liefst.
    public void run(String... strings) {
        Activity a = new Activity("Tuinfeest 2020", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Suspendisse tempus ipsum lacus, sit amet pharetra turpis congue at. Sed at rutrum enim, vel iaculis velit. Donec sem turpis, ultrices ut tortor rutrum, dapibus rhoncus risus. In porttitor elit at quam rhoncus, vel dictum arcu cursus.", LocalDateTime.of(2020, 11, 24, 14, 30), LocalDateTime.of(2020, 11, 24, 18, 30), true, "image1", 51,0.0, "Schooltuin");
        Activity b = new Activity("Willekeurige Activiteit in de toekomst", "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", LocalDateTime.of(2021, 12, 18, 19, 30), LocalDateTime.of(2021, 12, 18, 21, 00), true, "image2", 14,10.0, "Het Verboden Bos");
        Activity c = new Activity("Ongepubliceerde Activiteit", "Deze activiteit zou niet mogen opdagen in het gewone gebruikersscherm.", LocalDateTime.of(2020, 11, 12, 8, 0), LocalDateTime.of(2020, 11, 12, 9, 0), false, "image3", 0, 0.0, "Azkaban");
        Activity d = new Activity("Gepubliceerde Activiteit, maar in het verleden", "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", LocalDateTime.of(2020, 10, 18, 19, 30), LocalDateTime.of(2020, 10, 18, 21, 00), true, "image4", 14,10.0, "Hagrid's Hut");

        ApplicationUser user = new ApplicationUser();
        List<String>lijst = new ArrayList<>();
        lijst.add("Ikke");
        Registration r = new Registration(1,"google-oauth2|104754956967910925709",4,false,lijst,a);

        registrationRepository.save(r);

        activityRepository.save(a);
        activityRepository.save(b);
        activityRepository.save(c);
        activityRepository.save(d);

        List<Activity> activities = activityRepository.findAll();
        for(Activity activity : activities) {
            System.out.println("ID: " + activity.getId() + "/ NAME: " + activity.getName());
        }

        System.out.println("======================================================");
        System.out.println("Reservaties");
        List<Registration> registraties = registrationRepository.findAll();
    }

}