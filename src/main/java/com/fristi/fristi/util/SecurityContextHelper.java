package com.fristi.fristi.util;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;

public class SecurityContextHelper {

    public static String getCurrentUserId(){
        Jwt jwt = (Jwt) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String userId = (String)jwt.getClaim("sub");
        return userId;

    }

    public static String getManagementToken (){
        HttpResponse<JsonNode> response = null;
        try {
            response = Unirest.post("https://ayramis.eu.auth0.com/oauth/token")
                    .header("content-type", "application/json")
                    .body("{\"client_id\":\"EnUFFspgi1ecEHzE1TAJYZy1n0li2z96\",\"client_secret\":\"LtQSZ1kAaSB3avbuqGKpSAq8olPgqJQVJR4Bv8Ui2MfbKS3t19mpRHKfsjzX3bja\",\"audience\":\"https://ayramis.eu.auth0.com/api/v2/\",\"grant_type\":\"client_credentials\"}")
                    .asJson();

        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return (String)response.getBody().getObject().get("access_token");
    }
}
