package com.fristi.fristi.util;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.server.ResponseStatusException;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;


public class Auth0Helper {
    
    public static String getCurrentUserId() {
        Jwt jwt = (Jwt)SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        String userId = (String)jwt.getClaim("sub");
        return userId;
    }

    public static String getManagementAccessToken() {
        HttpResponse<JsonNode> response = null;
        try {
            response = Unirest.post("https://ayramis.eu.auth0.com/oauth/token")
                    .header("content-type", "application/json")
                    .body("{\"client_id\":\"EnUFFspgi1ecEHzE1TAJYZy1n0li2z96\",\"client_secret\":\"LtQSZ1kAaSB3avbuqGKpSAq8olPgqJQVJR4Bv8Ui2MfbKS3t19mpRHKfsjzX3bja\",\"audience\":\"https://ayramis.eu.auth0.com/api/v2/\",\"grant_type\":\"client_credentials\"}")
                    .asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return (String)response.getBody().getObject().get("access_token");
    }

    public static HttpResponse<String> callMgmtAPI(String url) {
        HttpResponse<String> response = null;

        try {
            do {
                response = Unirest.get(url)
                        .header("authorization", "Bearer " + getManagementAccessToken())
                        .asString();
                if(response.getStatus() > 200) {
                    System.out.println("Request: " + url + " // " + response.getStatus());
                    if(response.getStatus() == 429) {
                        Thread.sleep(2000);
                        continue;
                    }
                    throw new ResponseStatusException(HttpStatus.valueOf(response.getStatus()),
                        response.getStatusText());
                }

                System.out.println("Request: " + url + " // " + response.getStatus());
            } while(response.getStatus() == 429);
        } catch(UnirestException e) {
            e.printStackTrace();
        } catch(InterruptedException e) {
            e.printStackTrace();
        }

        return response;
    }

}
