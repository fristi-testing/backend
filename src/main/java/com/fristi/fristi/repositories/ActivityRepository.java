package com.fristi.fristi.repositories;

import java.time.LocalDateTime;
import java.util.List;

import com.fristi.fristi.models.Activity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ActivityRepository extends JpaRepository<Activity, Integer> {

    List<Activity> findByPublishedTrue();

    List<Activity> findByPublishedFalse();

    List<Activity> findByPublishedTrueAndEndDateAfter(LocalDateTime localDateTime);

    List<Activity> findByPublishedTrueAndEndDateBefore(LocalDateTime localDateTime);

    List<Activity> findByPriceIsLessThanEqual(double price);
}