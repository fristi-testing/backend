package com.fristi.fristi.repositories;

import com.fristi.fristi.models.Activity;
import com.fristi.fristi.models.Registration;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RegistrationRepository extends JpaRepository<Registration, Long> {

    List<Registration> findByActivityId(int activityId);

    List<Registration> findByActivity(Activity activity);

    // List<Registration> findByPaymentOffline(Activity activity);
}
