package com.fristi.fristi.models;
/*
import javax.persistence.*;
import java.util.List;

/**
 * Entity used to describe a Registration for an Activity
 *
 */

import javax.persistence.*;
import java.util.List;
import com.fristi.fristi.models.*;
@Entity
@Table(name = "registrations")
public class Registration {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name= "id")
    private long id;

    /*One to many Corresponding collection attribute should be implemented in the Application Model*/

    @Column(name="UserId",nullable = false)
    private String user;

    @Column(name = "extra_persons",columnDefinition = "integer default 0",nullable = false)
    private int amountOfExtraPersons;

    /*En table in de database die de registration mapped op de naam van elk extra Persoon =>  JPA embeddable types 
    https://www.callicoder.com/hibernate-spring-boot-jpa-element-collection-demo/
    */
    @ElementCollection
    @CollectionTable(name = "personen_registration", joinColumns = @JoinColumn(name = "registration_id"))
    @Column(name = "personName")
    private List<String> extraPersonen;

    @Column
    private Boolean paymentOffline;

    /*One to many Corresponding collection attribute should be implemented in the Activity Model*/
    @ManyToOne(targetEntity=Activity.class ,cascade = CascadeType.MERGE)
    @JoinColumn(name = "activityId", referencedColumnName = "id")
    Activity activity;

    public Registration() {}

    public Registration(long id,String user ,int amountOfExtraPersons, Boolean paymentOffline, List<String> extraPersonen, Activity activity) {
        this.id = id;
        this.user = user;
        this.amountOfExtraPersons = amountOfExtraPersons;
        this.paymentOffline = paymentOffline;
        this.extraPersonen = extraPersonen;
        this.activity = activity;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public int getAmountOfExtraPersons() {
        return amountOfExtraPersons;
    }

    public void setAmountOfExtraPersons(int amountOfExtraPersons) {
        this.amountOfExtraPersons = amountOfExtraPersons;
    }

    public Boolean getPaymentOffline() {
        return paymentOffline;
    }

    public void setPaymentOffline(Boolean paymentOffline) {
        this.paymentOffline = paymentOffline;
    }

    public List<String> getExtraPersonen() {
        return extraPersonen;
    }

    public void setExtraPersonen(List<String> extraPersonen) {
        this.extraPersonen = extraPersonen;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Registration{" +
                "id=" + id +
                ", amountOfExtraPersons=" + amountOfExtraPersons +
                ", extraPersonen=" + extraPersonen +
                ",userId= " + user +
                ", paymentOffline=" + paymentOffline +
                ", activity=" + activity +
                '}';
    }


}


