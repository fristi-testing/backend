package com.fristi.fristi.models;

import java.time.LocalDateTime;
import javax.persistence.*;

import java.util.LinkedList;
import java.util.List;

/**
 * Entity used to describe an activity
 */


@Table(name = "activities")
@Entity
public class Activity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(nullable = false)
    private String name;

    @Column(columnDefinition = "LONGTEXT")
    private String description;

    @Column(nullable = false)
    private LocalDateTime startDate;

    @Column
    private LocalDateTime endDate;

    @Column(nullable = false)
    private boolean published;

    @Column(nullable = true)
    private String imageLink;

    @Column
    private int amountOfLikes;

    @Column
    private double price;

    @Column
    private String location;

    /* Fetchtype lazy because all the registration can't always be accessible*/
    @OneToMany(targetEntity= Registration.class , mappedBy = "activity", fetch = FetchType.LAZY,cascade = CascadeType.ALL,
    orphanRemoval = true)
    private List<Registration> registrations = new LinkedList<>();

    //Constructor
    public Activity(){}

    /**
     * Constructs a new Activity
     *
     * @param name          the name of the activity
     * @param description   a description of the activity
     * @param startDate     the date the activity starts
     * @param endDate       the date the activity ends
     * @param published     identifies if an activity is published
     * @param imageLink     an image for the activity
     * @param amountOfLikes amount of likes an activity has
     * @param price         the price of the activity
     * @param location      the location of the activity
     */

    public Activity(String name, String description, LocalDateTime startDate, LocalDateTime endDate, boolean published, String imageLink, int amountOfLikes, double price, String location) {
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.published = published;
        this.imageLink = imageLink;
        this.amountOfLikes = amountOfLikes;
        this.price = price;
        this.location = location;
    }

    // Getter & Setter
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public int getAmountOfLikes() {
        return amountOfLikes;
    }

    public String getLocation() { return location; }

    public void setLocation(String location) { this.location = location; }

    // Doorgaans gebeurt dit telkens één per één? Dus kan wel verwerkt worden in de methode.
    public void setAmountOfLikes(int likesToAdd) {
        this.amountOfLikes = likesToAdd;
    }

    /*
    Komt erbij wanneer we de sponsors en reservaties gaan implementeren.
    
    @ManyToMany
    private List<Sponsor> sponsors;
 */

    @Override
    public String toString() {
        return "ID: " + this.id + "\t" + "name: " + this.name + "\t" + "Description: "
                + this.description + "\t" + "likes : " + this.amountOfLikes
                + "\t" + "Price: " + this.price
                + "\t" + "Location: " + this.location;
    }
}
