package com.fristi.fristi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FristiApplication {

	public static void main(String[] args) {
		SpringApplication.run(FristiApplication.class, args);
	}
}
