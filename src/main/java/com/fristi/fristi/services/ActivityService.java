package com.fristi.fristi.services;

import java.time.LocalDateTime;
import java.util.List;

import com.fristi.fristi.exceptions.ActivityNotFoundException;
import com.fristi.fristi.models.Activity;
import com.fristi.fristi.repositories.ActivityRepository;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ActivityService {

    @Autowired
    private ActivityRepository activityRepository;

    public Activity createActivity(Activity activity) {
        return activityRepository.save(activity);
    }

    public Activity updateActivity(Activity activity) {
        Optional<Activity> foundActivity = activityRepository.findById(activity.getId());

        if (foundActivity.isPresent()) {
            System.out.println("activity is found");

            Activity updatedActivity = foundActivity.get();

            // welke attributen kunnen er precies aangepast worden?

            updatedActivity.setName(activity.getName());
            updatedActivity.setStartDate(activity.getStartDate());
            updatedActivity.setEndDate(activity.getEndDate());
            updatedActivity.setLocation(activity.getLocation());
            updatedActivity.setPrice(activity.getPrice());
            updatedActivity.setDescription(activity.getDescription());
            updatedActivity.setImageLink(activity.getImageLink());

            activityRepository.save(updatedActivity);
            return updatedActivity;
        } else {
            // TODO: Eventueel zelf nieuwe exception type definiëren en hergebruiken.
            throw new RuntimeException("Activiteit met id : " + activity.getId() + " niet gevonden.");
        }
    }

    /**
     * To get activities that are published and not in the past
     *
     * @return published activities not in the past
     */
    public List<Activity> getPublishedActivitiesAndEndDateAfter() {

        return activityRepository.findByPublishedTrueAndEndDateAfter(LocalDateTime.now());
    }

    public void deleteActivity(int activityId) {
        Optional<Activity> activityToDelete = activityRepository.findById(activityId);

        if (activityToDelete.isPresent()) {
            activityRepository.delete(activityToDelete.get());
        } else {
            throw new RuntimeException("Activiteit met id : " + activityId + " niet gevonden.");
        }
    }

    /**
     * To get all activities
     *
     * @return All activities
     */
    public List<Activity> getAllActivities() {
        return activityRepository.findAll();
    }

    /**
     * To get activities that are published
     *
     * @return published activities
     */
    public List<Activity> getPublishedActivities() {
        return activityRepository.findByPublishedTrue();
    }

    public List<Activity> getNonPublishedActivities() {
        return activityRepository.findByPublishedFalse();
    }

    public List<Activity> getPastActivities() {
        return activityRepository.findByPublishedTrueAndEndDateBefore(LocalDateTime.now());
    }

    public List<Activity> getFreeActivities() {
        return activityRepository.findByPriceIsLessThanEqual(0.0);
    }


    /**
     * To get an activity by ID
     *
     * @param activityId Id that will be used to lookup an activity
     * @return an Activity
     */
    public Activity getActivityById(int activityId) {
        Optional<Activity> foundActivity = activityRepository.findById(activityId);

        if (foundActivity.isPresent()) {
            return foundActivity.get();
        } else {
//            throw new RuntimeException("Activiteit met id : " + activityId + " niet gevonden.");
            throw new ActivityNotFoundException();
        }
    }

        /**
     * To get activities that are published and not in the past
     *
     * @return published activities not in the past
     */


}