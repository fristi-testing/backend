package com.fristi.fristi.services;

import com.fristi.fristi.exceptions.RegistrationNotFoundException;
import com.fristi.fristi.models.Registration;
import com.fristi.fristi.repositories.RegistrationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class RegistrationService {

    @Autowired
    private RegistrationRepository registrationRepository;

    public Registration createRegistration(Registration registration){return registrationRepository.save(registration);}

    public void deleteRegistration(long registrationId) {
        Optional<Registration> registrationToDelete = registrationRepository.findById(registrationId);

        if (registrationToDelete.isPresent()) {
            registrationRepository.delete(registrationToDelete.get());
        } else {
            throw new RuntimeException("Registratie met id : " + registrationId + " niet gevonden.");
        }
    }

    public List<Registration> getAllRegistrations() {
        return registrationRepository.findAll();
    }


    public Registration getRegistrationById(long registrationId) {
        Optional<Registration> foundRegistration = registrationRepository.findById(registrationId);

        if (foundRegistration.isPresent()) {
            return foundRegistration.get();
        } else {
            throw new RegistrationNotFoundException();
        }
    }

    public List<Registration> getRegistrationByActivityId(int id){
        List<Registration> foundRegistrations = registrationRepository.findByActivityId(id);
    return foundRegistrations;
    }
   // public List<Registration>getPaidRegistrations(){return registrationRepository.findByPaymentOffline()}
}
