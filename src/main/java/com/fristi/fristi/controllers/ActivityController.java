package com.fristi.fristi.controllers;
import java.util.List;
import com.fristi.fristi.models.Activity;
import com.fristi.fristi.services.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/api/activities")
@CrossOrigin(origins = {"http://localhost:3000", "https://fristi-testing-staging-fe.herokuapp.com"})
@RestController
public class ActivityController {
    
    @Autowired
    private ActivityService activityService;

    @GetMapping
    @ResponseBody
    // Betere manier dan de published-status in de request params te verkrijgen?
    public ResponseEntity<List<Activity>> getActivities(@RequestParam(required = false) boolean published) {
        if(published){ return ResponseEntity.ok().body(activityService.getPublishedActivities());}
        else {
            return ResponseEntity.ok().body(activityService.getAllActivities());
        }
    }

    @GetMapping("/upcoming")
    public ResponseEntity<List<Activity>> getPublishedActivitiesAndEndDateAfter()  {
        return ResponseEntity.ok().body(activityService.getPublishedActivitiesAndEndDateAfter());
    }

    @GetMapping("/nonpublished")
    public ResponseEntity<List<Activity>> getNonPublishedActivities()  {
        return ResponseEntity.ok().body(activityService.getNonPublishedActivities());
    }

    @GetMapping("/past")
    public ResponseEntity<List<Activity>> getPastActivities()  {
        return ResponseEntity.ok().body(activityService.getPastActivities());
    }

    public ResponseEntity<List<Activity>> getFreeActivities(@RequestParam(required = false, defaultValue = "0.0") double price) {
        if(price == 0.0){ return ResponseEntity.ok().body(activityService.getFreeActivities());}
        else {
            return ResponseEntity.ok().body(activityService.getAllActivities());
        }

        // TODO: Eens users zijn geïmplementeerd ook enkel de niet-gepubliceerde activiteiten tonen.
    }


    @GetMapping("/{id}")
    public ResponseEntity<Activity> getActivityById(@PathVariable int id)  {
        System.out.println("In ActivityController - {id}\t");
        return ResponseEntity.ok().body(activityService.getActivityById(id));
    }

    @PostMapping("/{id}")
    public ResponseEntity editActivity(@RequestBody Activity activity) {
        return ResponseEntity.ok().body(activityService.updateActivity(activity));
    }

    @PostMapping
    public ResponseEntity createActivity(@RequestBody Activity activity) {
        return ResponseEntity.ok().body(activityService.createActivity(activity));
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity deleteActivity(@PathVariable int id) {
        activityService.deleteActivity(id);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }
}
