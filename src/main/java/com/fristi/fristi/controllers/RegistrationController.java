package com.fristi.fristi.controllers;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;
import com.fristi.fristi.models.Activity;
import com.fristi.fristi.models.ApplicationUser;
import com.fristi.fristi.models.Registration;
import com.fristi.fristi.services.ActivityService;
import com.fristi.fristi.services.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

//@RequestMapping("/api/registrations")
// GETEST IN BROWSER OK WERKT
@RequestMapping("/api/reservations")
@CrossOrigin(origins = {"http://localhost:3000", "https://fristi-testing-staging-fe.herokuapp.com"})
@RestController
public class RegistrationController {

    @Autowired
    RegistrationService registrationService;
    
    @Value("${auth0.domain}")
    String domain;

    Activity activiteit;

    @GetMapping
    @ResponseBody
    public ResponseEntity<List<Registration>> getRegistrations() {
        System.out.println("De registraties: \t" + registrationService.getAllRegistrations());
        return ResponseEntity.ok().body(registrationService.getAllRegistrations());
    }

    //      /api/reservations/1
    // GETEST IN BROWSER OK WERKT

    @GetMapping("/{id}")
    @ResponseBody()
    public Registration getRegistration (@PathVariable int id){

        Registration registration = registrationService.getRegistrationById(id);

        System.out.println("in RegistrationController - getRegistration\t:" + registration.toString() );
        return registration;
    }

    /**
     * Geeft lijst van registraties weer voor de activiteit
     * @param id identificatie van de activiteit
     * @return Gefft een lijst van registraties weer
     */


    //TODO registratie per activityId zetten ipv Activity Object
    // Misschien enkel een activityId in het model Registration zetten

    //      /api/reservations/activity/1

    @GetMapping("/activity/{id}")
    @ResponseBody()
    public List<Registration> getRegistrations(@PathVariable int id) {
        List<Registration> registrations = registrationService.getRegistrationByActivityId(id);
        return registrations;
    }


//TODO registratie per userId zetten ipv User Object
    // GETEST IN BROWSER NIETOK WERKT NIET


    @PostMapping()
    public ResponseEntity<Registration> createRegistration(@RequestBody Registration registration,@RequestHeader (name="Authorization") String access_token){
        HttpHeaders headers = new HttpHeaders();
        String token = (String) access_token.subSequence(6, access_token.length());
        headers.set("Authorization", "Bearer " + token);
        HttpEntity<ApplicationUser> entity = new HttpEntity<ApplicationUser>(headers);

        // Use the "RestTemplate" API provided by Spring to make the HTTP request
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<ApplicationUser> user = restTemplate.exchange("https://" + domain + "/userinfo", HttpMethod.GET, entity, ApplicationUser.class);
        registration.setUser(user.getBody().getSub());
        return ResponseEntity.ok().body(registrationService.createRegistration(registration));
    }

    @DeleteMapping("/students/{id}")
public void deleteStudent(@PathVariable long id) {
	registrationService.deleteRegistration(id);
}
    /*
    /api/registraties - all registraties
    api/activiteit/1/registraties - findByactiviteit
    /api/registraties/{userId}

     */
}

