package com.fristi.fristi.controllers;
import com.fristi.fristi.util.SecurityContextHelper;
import org.json.JSONObject;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import java.util.Map;
import com.fristi.fristi.util.Auth0Helper;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;


@RequestMapping("/api/users")
@CrossOrigin(origins = {"http://localhost:3000", "https://fristi-testing-staging-fe.herokuapp.com"})
@RestController
public class UserController {

    @Value("https://ayramis.eu.auth0.com/api/v2")
    String baseMgmtUrl;

    @GetMapping("/profile")
    public String getUserDetails() {

        String userId = SecurityContextHelper.getCurrentUserId();
        String token = SecurityContextHelper.getManagementToken();
        HttpResponse<String> response = null;
        try {
            response = Unirest.get(baseMgmtUrl + "/users/" + userId)
                    .header("authorization", "Bearer " + token)
                    .asString();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return response.getBody();
    }

    @PatchMapping("/profile")
    public String patchUserDetails(@RequestBody Map<String, Object> updatedUser) {

        JSONObject userJson = new JSONObject(updatedUser);
        for (Map.Entry<String, Object> stringObjectEntry : updatedUser.entrySet()) {
            System.out.println(stringObjectEntry.getKey() + " : value: " + stringObjectEntry.getValue());
        }
        String userId = SecurityContextHelper.getCurrentUserId();
        String token = SecurityContextHelper.getManagementToken();
        HttpResponse<String> response = null;
        try {
            response = Unirest.patch(baseMgmtUrl + "/users/" + userId)
                    .header("content-type", "application/json")
                    .header("authorization", "Bearer " + token)
                    .header("cache-control", "no-cache")
                    .body(userJson)
                    .asString();

        } catch (UnirestException e) {
            e.printStackTrace();
        }
        System.out.println("responsestatus: " + response.getStatus() + " body: " + response.getBody() + " statustext: " + response.getStatusText());
        return response.getBody();

    }

    @DeleteMapping("/{id}")
    public String deleteUser(@PathVariable String id) {
        HttpResponse<String> response = null;
        try {
            response = Unirest.delete("https://ayramis.eu.auth0.com/api/v2/users/" + id)
                    .header("authorization", "Bearer " + Auth0Helper.getManagementAccessToken())
                    .asString();
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        System.out.println(response.getStatus() + " / " + response.getStatusText());
        System.out.println(response.getBody());
        return response.getBody();
    }

    //TODO: Non-GET Unirest requests in callMgmtAPI verwerken.

    @GetMapping("")
    public String getUsers() {

        HttpResponse<String> response = Auth0Helper.callMgmtAPI(baseMgmtUrl + "/users");
        return response.getBody();
    }

    @GetMapping("/roles")
    public String getRoles() {

        HttpResponse<String> response = Auth0Helper.callMgmtAPI(baseMgmtUrl + "/roles");
        return response.getBody();
    }

    @GetMapping("/{id}/roles")
    public String getUserRoles(@PathVariable String id) {

        HttpResponse<String> response = Auth0Helper.callMgmtAPI(baseMgmtUrl + "/users/" + id + "/roles");
        return response.getBody();
    }

    @GetMapping("/user/{id}")
    public String getUser(@PathVariable String id) {

        HttpResponse<String> response = Auth0Helper.callMgmtAPI(baseMgmtUrl + "/users/" + id);
        return response.getBody();
    }

    @PostMapping("/{id}/roles")
    public String addUserRoles(@RequestBody Map<String, Object> updatedRoles, @PathVariable String id) {
        JSONObject updateJson = new JSONObject(updatedRoles);
        System.out.println("Test");
        HttpResponse<String> response = null;
        try {
            response = Unirest.post("https://ayramis.eu.auth0.com/api/v2/users/" + id + "/roles")
                    .header("content-type", "application/json")
                    .header("authorization", "Bearer " + Auth0Helper.getManagementAccessToken())
                    .body(updateJson)
                    .asString();
            if(response.getStatus() > 200) {
                throw new ResponseStatusException(HttpStatus.valueOf(response.getStatus()),
                    response.getStatusText());
            }
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        System.out.println(response.getStatus() + " / " + response.getStatusText());
        System.out.println(response.getBody());
        return response.getBody();
    }

    @DeleteMapping("/{id}/roles")
    public String removeUserRoles(@RequestBody Map<String, Object> updatedRoles, @PathVariable String id) {
        JSONObject updateJson = new JSONObject(updatedRoles);
        HttpResponse<String> response = null;
        try {
            response = Unirest.delete("https://ayramis.eu.auth0.com/api/v2/users/" + id + "/roles")
                    .header("content-type", "application/json")
                    .header("authorization", "Bearer " + Auth0Helper.getManagementAccessToken())
                    .body(updateJson)
                    .asString();
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        System.out.println(response.getStatus() + " / " + response.getStatusText());
        System.out.println(response.getBody());
        return response.getBody();
    }



}
